import 'package:flutter/material.dart';
import './reviews.dart';

class Result extends StatelessWidget {
  final int totalScore;
  final Function resetHandler;

  Result(this.totalScore, this.resetHandler);

  String get resultPhrase {
    String resultText;
    if (totalScore > 26) {
      resultText = 'You are awesome';
    } else if (totalScore >= 22 && totalScore <= 26) {
      resultText = 'You are good';
    } else {
      resultText = 'You are not so good.';
    }
    return resultText;
  }

  String get resultScore {
    String resultScore;
    resultScore = totalScore.toString();

    return resultScore;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Text(
            resultPhrase,
            style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: Colors.black87),
          ),
          Text('Your Score is'),
          Text(
            resultScore,
            style: TextStyle(color: Colors.blue[600], fontSize: 24),
          ),
          FlatButton(
            onPressed: resetHandler,
            color: Colors.blue,
            child: Text(
              'Reset Quiz',
              style: TextStyle(color: Colors.white),
            ),
          ),
          Review(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.max,
            children: [
              Button1(),
              Button2(),
            ],
          )
        ],
      ),
    );
  }
}

class Button1 extends StatelessWidget {
  const Button1({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 100,
      height: 40,
      child: FlatButton(
        onPressed: () {
          Navigator.of(context).pushNamed('/stack', arguments: false);
        },
        child: Text(
          'Go to Stack',
          style: TextStyle(color: Colors.white),
        ),
        color: Colors.blue[700],
      ),
    );
  }
}

class Button2 extends StatelessWidget {
  const Button2({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 40,
      width: 100,
      child: FlatButton(
        onPressed: () {
          Navigator.of(context).pushNamed('/stack', arguments: true);
        },
        child: Text(
          'Go to Card',
          style: TextStyle(color: Colors.white),
        ),
        color: Colors.blue[700],
      ),
    );
  }
}
