import 'package:basic_app/quiz_app/quiz.dart';
import 'package:flutter/material.dart';
import './quiz.dart';
import './result.dart';

class MyQuizApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyQuizAppState();
  }
}

class _MyQuizAppState extends State<MyQuizApp> {
  final _questions = const [
    {
      'questionText': 'What\'s your fav Color?',
      'answers': [
        {'text': 'Red', 'score': 8},
        {'text': 'White', 'score': 2},
        {'text': 'Orange', 'score': 5},
        {'text': 'Black', 'score': 10}
      ]
    },
    {
      'questionText': 'What\'s your fav Car?',
      'answers': [
        {'text': 'Mercedes', 'score': 8},
        {'text': 'BMW', 'score': 10},
        {'text': 'AUDI', 'score': 5},
        {'text': 'Range-Rover', 'score': 9}
      ]
    },
    {
      'questionText': 'What\'s your fav framework?',
      'answers': [
        {'text': 'Angular', 'score': 8},
        {'text': 'React.js', 'score': 8},
        {'text': 'Flutter', 'score': 9},
        {'text': 'Ionic', 'score': 5}
      ]
    },
  ];
  var _questionIndex = 0;
  var _totalScore = 0;
  var _currentIndex = 0;

  void _resetQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }

  void _answerToQuestion(int score) {
    setState(() {
      _questionIndex = _questionIndex + 1;
      _totalScore += score;
    });
    print(_questionIndex);

    if (_questionIndex < _questions.length) {
      print('we have more question');
    } else {
      print('No more questions.');
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Quiz App'),
        backgroundColor: Colors.blue[700],
      ),
      body: _questionIndex < _questions.length
          ? Quiz(
              answerToQuestion: _answerToQuestion,
              questionIndex: _questionIndex,
              questions: _questions,
            )
          : Result(_totalScore, _resetQuiz),
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentIndex,
          type: BottomNavigationBarType.fixed,
          selectedFontSize: 18,
          unselectedFontSize: 14,
          onTap: (int index) {
            setState(() {
              _currentIndex = index;
            });
          },
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.home),
                label: 'Home',
                backgroundColor: Colors.blue[700]),
            BottomNavigationBarItem(
                icon: Icon(Icons.search),
                label: 'Search',
                backgroundColor: Colors.blue[700]),
            BottomNavigationBarItem(
                icon: Icon(Icons.camera),
                label: 'Camera',
                backgroundColor: Colors.blue[700]),
            BottomNavigationBarItem(
                icon: Icon(Icons.person),
                label: 'Profile',
                backgroundColor: Colors.blue[700])
          ]),
    );
  }
}
