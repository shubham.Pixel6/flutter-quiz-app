import 'package:flutter/material.dart';
import './card_stack.dart';
import './quiz_app/quiz_app.dart';

class RouteGenrator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;

    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => MyQuizApp());
      case '/stack':
        if (args is bool) {
          print(args);
          print(settings);
          return MaterialPageRoute(builder: (_) => MyApp2(data: args));
        }
        return _errorRoute();
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error Page'),
        ),
        body: Center(
          child: Text('Error Occured.'),
        ),
      );
    });
  }
}
